﻿using Newtonsoft.Json;

namespace Reflection.Serialization
{
    class JsonSerialization: ISerialization
    {
        string Serialize<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        T Deserialize<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }
    }
}
