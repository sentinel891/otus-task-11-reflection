﻿namespace Reflection.Serialization
{
    public interface ISerialization
    {
        public string Serialize(object obj) 
        {
            return "";
        }

        public T Deserialize<T>(string data) 
        {
            return default(T);
        }
    }
}
