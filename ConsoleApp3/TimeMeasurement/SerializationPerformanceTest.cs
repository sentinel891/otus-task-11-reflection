﻿using Reflection.Serialization;
using System;
using System.Diagnostics;
using System.IO;

namespace Reflection.TimeMeasurement
{
    class SerializationPerformanceTest
    {
        const string FileName = @"C:\temp\serializationTest.txt";
        public long SerializationTime { get; set; }
        public long DeserializationTime { get; set; }
        public void Test<SerializeClass>(SerializeClass obj, ISerialization serializator, int iterations)
        {
            var SerializedObj = serializator.Serialize(obj);

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            for (int i = 0; i < iterations; i++)
            {
                SerializedObj = serializator.Serialize(obj);
                File.WriteAllText(FileName, SerializedObj);
            }
            stopWatch.Stop();
            SerializationTime = stopWatch.ElapsedMilliseconds;

            stopWatch = new Stopwatch();
            stopWatch.Start();
            for (int i = 0; i < iterations; i++)
            {
                SerializedObj = File.ReadAllText(FileName);
                var DeserealizaedObj = serializator.Deserialize<SerializeClass>(SerializedObj);
            }
            stopWatch.Stop();
            DeserializationTime = stopWatch.ElapsedMilliseconds;
        }
    }
}
