﻿using Reflection.Serialization;
using Reflection.TimeMeasurement;
using System;

namespace Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            int iterations = 10000;

            SerializationPerformanceTest test = new SerializationPerformanceTest();

            test.Test(F.Get(), new CsvSerialization(), iterations);
            Console.WriteLine("CSV serializator, " + iterations + " iterations: " + test.SerializationTime + " milliseconds");
            Console.WriteLine("CSV deserializator, " + iterations + " iterations: " + test.DeserializationTime + " milliseconds");

            test.Test(F.Get(), new JsonSerialization(), iterations);
            Console.WriteLine("JSON serializator, " + iterations + " iterations: " + test.SerializationTime + " milliseconds");
            Console.WriteLine("JSON deserializator, " + iterations + " iterations: " + test.DeserializationTime + " milliseconds");

            Console.ReadLine();
        }
    }
}
